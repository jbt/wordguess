#!/usr/bin/env python3

from subprocess import DEVNULL
from subprocess import PIPE
from subprocess import Popen
from sys import argv

if __name__ == '__main__':
    print( ' '.join(argv) )
    exe = argv[1]
    doxyfile = argv[2]
    fail = False
    p = Popen( [exe, doxyfile], stdout=DEVNULL, stderr=PIPE )
    for l in p.stderr.read().decode().split('\n'):
        if 'error' in l or ': warning: ' in l:
            print( l.replace(':error: warning: ', ':error: ').replace('warning:', 'error:') )
            fail = True
        else:
            print( l )
    if fail:
        exit( 1 )

