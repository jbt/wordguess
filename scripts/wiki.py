#!/usr/bin/env python3

#gitpython requires python2
from shutil import copy2
from subprocess import call
from subprocess import check_call
from subprocess import PIPE
from subprocess import Popen 
from sys import argv
from os import makedirs
from os import path
from os import walk

def pull( wikirepo ):
    check_call( ['git', '-C', wikirepo, 'pull'], stdout=PIPE, timeout=99 )

def clone_wiki( mainrepo, wikirepo ):
    p = Popen( ['git', '-C', mainrepo, 'config', '--get', 'remote.origin.url'], stdout=PIPE, timeout=9 )
    main_url = p.stdout.read().decode().strip()
    wiki_url = main_url.replace( '.git', '.wiki.git' )
    print( main_url, wiki_url, wikirepo )
    check_call( ['git', 'clone', wiki_url, wikirepo], timeout=9 )
    
def copy_files_over( fd, td ):
    rv = []
    for root, dirs, files in walk( fd ):
        for f in files:
            p = path.join( root, f )
            pr = path.relpath( p, fd )
            t = path.join(td, pr)
            makedirs( path.dirname(t), exist_ok = True )
            copy2( p, t )
            rv.append( pr )
    return rv

def add_commit_push( wikidir, files_to_add ):
    check_call( ['git', '-C', wikidir, 'add'] + files_to_add, timeout=9 )
    call( ['git', '-C', wikidir, 'commit', '-m', 'wiki.py - automated doc update from build'], timeout=9 )
    check_call( ['git', '-C', wikidir, 'push'], timeout=99 )

if __name__ == '__main__':
    srcdir = argv[1]
    bindir = argv[2]
    wikidir = path.join(bindir,'wiki')
    if path.isdir( wikidir ):
        pull( wikidir )
    else:
        clone_wiki( srcdir, wikidir )
    htmldir = path.join( bindir, 'html' )
    files_to_add = copy_files_over( htmldir, wikidir )
    copy2( path.join(srcdir,'README.md'), wikidir )
    files_to_add.append( 'README.md' )
    copy2( path.join(srcdir,'README.md'), path.join(wikidir, 'home.md') )
    files_to_add.append( 'home.md' )
    #print( files_to_add )
    add_commit_push( wikidir, files_to_add )
