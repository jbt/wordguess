#!/usr/bin/env python3

import re
from os import path, listdir
from subprocess import check_output
from sys import argv

def find_oneline_lambda_offsets( source_text ):
    rv = []
    #there's a number of syntaces I'm ignoring here, like trailing return type. Those guys can just face clang-format!
    pat = re.compile( r'\[[^(){};]*\]\s*\([^(){};]*\)\s*{[^;]*;[^;}]*}' )
    off = 0
    while off < len(source_text):
        s = source_text[off:]
        match = pat.search( s )
        if match:
            rv.append( [off+match.start(), match.end()-match.start()] )
            off += match.end()
            #print( 'Found one:', match.group() )
        else:
            break
    return rv

def find_oneline_lambdas_in_file( path ):
    with open( path ) as file:
        return find_oneline_lambda_offsets(file.read())
    return rv

def transform_file( path, skipped_sections ):
    off = 0
    args = [ 'clang-format', '-i', path ]
    elbow_room = 88
    for s in skipped_sections:
        if off < s[0] - elbow_room:
            args.append( '--offset='+str(off) )
            args.append( '--length='+str(s[0]-off-elbow_room) )
        off = s[0] + s[1] + elbow_room
    print( args )
    print( check_output(args) )

def handle_path( p ):
    if path.isfile(p):
        skipped_sections = find_oneline_lambdas_in_file( p )
        transform_file( p, skipped_sections )
    elif path.isdir(p):
        for e in listdir(p):
            handle_path( path.join(p, e) )
    else:
        raise Exception(p+' DNE')

if __name__ == '__main__':
    for arg in argv[1:]:
        handle_path( arg )
