#!/usr/bin/env python3

from os         import path
from subprocess import check_output
from sys        import argv
from tempfile   import NamedTemporaryFile

git_hash = check_output( ['git', 'log', '--max-count=1', '--format=%H', '--color=never'], timeout=1 ).decode().strip().replace('\n','')
mods = check_output( ['git', 'status', '--porcelain'], timeout=1 ).decode().replace('\n', '\\n  ')
#print( 'Hash=', git_hash, 'mods=', mods )
content =  '#include <jbt/String.hpp> \n\n'
content +=  '#include <boost/version.hpp> \n\n'
content +=  'namespace wg\n{\n    namespace {\n\n'
content += '        auto VERSION_STRING = "WordGuess version ' + argv[2] + ' "\n'
content += '                              "(' + git_hash + ')"\n'
if len(mods) > 0:
    content += '        " \\n                              #### THIS BUILD IS DIRTY !!! ####\\n  '+ mods+ ' "\n'
content += '                              " Boost=" +jbt::to_string(BOOST_VERSION)+ "\\n" '
content += '                              ;\n    }\n}\n\n'

if path.isfile(argv[1]):
    with open(argv[1]) as existing:
        old = existing.read()
        if old == content:
            exit(0)
            
with open(argv[1], 'w') as out:
    print('Updating',argv[1])
    out.write( content )
