import errno
import subprocess

def exists( prog ):
    try:
        return subprocess.call( [prog, '--version'], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL ) == 0
    except OSError as e:
        if e.errno == errno.ENOENT:
            return False
        else:
            raise
