#!/usr/bin/env python3

from test_util import exists
import os
import re
from subprocess import *
from sys import argv

print( ' '.join(argv) )

if exists('valgrind'):
    cmd = ['valgrind', '--error-exitcode=99', '--log-file='+argv[1]+'.valgrind.out']
else:
    cmd = []

cmd.append( argv[1] )
cmd.append( '--log_level=message' )

print( ' '.join(cmd) )
p = Popen( cmd, stdout=PIPE, stderr=PIPE )
ec = p.wait()
#/home/jturpish/projects/wordguess/test/unit/Dictionary.cpp(74): error in "read_from_file_does_as_expected": check d.contents_ == "ABC\nDEF\n" failed [ != ABC
r = re.compile('(.+[a-z]+\.cpp)\(([1-9][0-9]*)\): (fatal )?(error|warning)(.+)')
for l in p.stdout.read().decode().split('\n'):
   m = r.match( l )
   if m:
      g=m.groups()
      # print( l, ' => ', g )
      if g[2]:
         f=g[2]
      else:
         f=''
      print( g[0]+':'+g[1]+':', g[3]+':', f, g[3], g[4] )
   else:
      print( l )
print( p.stderr.read().decode() )
if ec != 0:
   os.remove( argv[1] )
exit( ec )
