#!/usr/bin/env python3

import test_util

import filecmp
from os import path
import subprocess
from sys import argv
import tempfile



perf = len(argv) > 3 and argv[3] == '--perf'

if perf:
    log = argv[1] + '.cachegrind.out'
else:
    log = argv[1] + '.memcheck.out'

if test_util.exists('valgrind'):
    cmd = [ 'valgrind', '--error-exitcode=99', '--log-file='+log ]
    if perf:
        cmd.append( '--tool=cachegrind' )
else:
    cmd = []

userwords = tempfile.NamedTemporaryFile()

cmd += [ argv[1], '--no-system-directories', '--directory', path.join(argv[2],'data'), '--seed=9', '--userwords='+userwords.name ]
#< data/verycommonwords.txt > dbg/out

inp = open( path.join(argv[2],'data','verycommonwords.txt') )
out = tempfile.NamedTemporaryFile()
print( ' '.join(cmd), '<', inp.name, '>', out.name )
ec = subprocess.call( cmd, stdin=inp, stdout=out, timeout=600 )
if ec != 0:
    print( 'Process failed with exit code', ec )
    exit( ec )

expected = path.join(argv[2],'test','smoke.out')
print( 'diff', out.name, expected )
if filecmp.cmp( out.name, expected, shallow=False ):
    if perf:
        with open(log) as lf:
            for line in lf.readlines():
                if '== LL misses: ' in line:
                    misses = int( line.split(':')[1].split('(')[0].replace(',', '') )
                    break;
        cms = path.join( path.dirname(argv[1]), 'cache_misses.txt' )
        if path.isfile(cms):
            with open(cms) as cmf:
                prev = int(cmf.read())
            if misses > prev * 1.01:
                print('Regression! We previously had', prev, 'lower-level cache misses and now we have', misses )
                exit(1)
            else:
                with open(cms, 'w') as cmf:
                    print( misses, file=cmf )
        else:
            with open(cms, 'w') as cmf:
                print( misses, file=cmf )
    print( 'Successed!' )
else:
    print( 'Files:', out.name, 'and', expected, 'differ.' )
    subprocess.call( ['meld', out.name, expected], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL )
    exit( 1 )
