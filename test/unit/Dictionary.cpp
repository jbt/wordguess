#include "wg/Dictionary.hpp"
#define BOOST_TEST_MODULE Dictionary
#include <boost/test/unit_test.hpp>
#include <boost/filesystem.hpp>

using namespace wg;
using namespace boost::filesystem;

BOOST_AUTO_TEST_CASE( can_construct )
{
    BOOST_CHECK_NO_THROW(Dictionary d;);
}


BOOST_AUTO_TEST_CASE( obscurity_based_on_earliest_dictionary_and_how_many )
{
    Dictionary d;
    std::istringstream s { "DEF\nABC\n" };
    d.read( s );
    s.clear();
    s.str( "DEF\nGHI\n" );
    d.read( s );
    auto l = d.finished();
    BOOST_CHECK_EQUAL(d.frequency(0),1);//DEF
    BOOST_CHECK_EQUAL(d.frequency(1),1);//ABC
    BOOST_CHECK_EQUAL(d.frequency(2),0);
    BOOST_CHECK_EQUAL(d.frequency(3),1);//GHI
    BOOST_CHECK_EQUAL(d.frequency(std::numeric_limits<Dictionary::obscure_t>::max()),0);
    BOOST_REQUIRE_GE(l.size(),1);
    auto p = l.at( 0UL );
    BOOST_CHECK_EQUAL(p.first,"ABC");
    BOOST_CHECK_EQUAL(p.second,1);
    BOOST_REQUIRE_GE(l.size(),2);
    p = l.at( 1UL );
    BOOST_CHECK_EQUAL(p.first,"DEF");
    BOOST_CHECK_EQUAL(p.second,0);
    BOOST_REQUIRE_EQUAL(l.size(),3);
    p = l.at( 2UL );
    BOOST_CHECK_EQUAL(p.first,"GHI");
    BOOST_CHECK_EQUAL(p.second,3);
}

BOOST_AUTO_TEST_CASE( default_files_includes_critical_values )
{
    auto p = Dictionary::default_files();
    std::vector<std::string> s { p.size() };
    std::transform( p.begin(), p.end(), s.begin(), [](auto x) {
        return x.string();
    } );
    auto m = std::accumulate( s.begin(), s.end(), std::string {"default files contained:"} );
    BOOST_CHECK_MESSAGE(std::find(s.begin(),s.end(),"userwords.txt")!=s.end(),m);//This program writes this file
    BOOST_CHECK_MESSAGE(std::find(s.begin(),s.end(),"words")!=s.end(),m);//The standard name for the system-installed dictionary
}

BOOST_AUTO_TEST_CASE( read_from_file_does_as_expected )
{
    auto p = jbt::fs::temporary_file();
    BOOST_TEST_MESSAGE( p );
    {
        std::ofstream f { p.string() };
        f << "ABC\nDEF\n";
    }
    struct
    {
        std::string contents_;
        bool read( std::ifstream& f )
        {
            contents_.resize( 11 );
            f.read( const_cast<char*>(contents_.data()), contents_.size() );
            contents_.resize( f.gcount() );
            return false;
        }
    } d;
    wg::read( d, p );
    BOOST_CHECK_EQUAL(d.contents_,"ABC\nDEF\n");
    // BOOST_FAIL("HERE!");
}
