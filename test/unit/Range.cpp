#include <jbt/Range.hpp>
#define BOOST_TEST_MODULE Range
#include <boost/test/unit_test.hpp>

BOOST_AUTO_TEST_CASE(SortedRangeView_contains)
{
    auto r = std::vector<int> { 1, 6, 9 };
    auto s = jbt::certify_sorted( r );
    bool c = s.contains(0);
    BOOST_CHECK_EQUAL(false,c);
    BOOST_CHECK_EQUAL(true ,s.contains(1));
    BOOST_CHECK_EQUAL(false,s.contains(2));
    BOOST_CHECK_EQUAL(false,s.contains(5));
    BOOST_CHECK_EQUAL(true ,s.contains(6));
    BOOST_CHECK_EQUAL(false,s.contains(7));
    BOOST_CHECK_EQUAL(false,s.contains(8));
    BOOST_CHECK_EQUAL(true ,s.contains(9));
}
