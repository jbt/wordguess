#include <jbt/FileSystem.hpp>
#define BOOST_TEST_MODULE FileSystem
#include <boost/test/unit_test.hpp>

BOOST_AUTO_TEST_CASE(returns_home_env)
{
    ::setenv( "HOME", "/home/john", false );
    BOOST_REQUIRE(std::getenv("HOME"));
    BOOST_REQUIRE_EQUAL(jbt::fs::home().string(),std::getenv("HOME"));
}
