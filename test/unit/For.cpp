#include "jbt/For.hpp"
#define BOOST_TEST_MODULE For
#include <boost/test/unit_test.hpp>

using namespace jbt::For;

template<class Auto>
std::ostream& operator<<( std::ostream& o, Auto& collection )
{
    o << '{';
    auto semi = false;
    for ( auto& e : collection )
    {
        if ( semi )
        {
            o << ';';
        }
        else
        {
            semi = true;
        }
        o << e;
    }
    return o << '}';
}

BOOST_AUTO_TEST_CASE( cross_ints )
{
    using V = std::vector<int>;
    V out;
    auto f = [&out](int i,int j) {
        out.push_back(i*10+j);
    };
    cross( V {1, 2, 3}, V {4, 5}, f );
    auto expected = {14,15,24,25,34,35};
    BOOST_CHECK_EQUAL_COLLECTIONS( out.begin(), out.end(), expected.begin(), expected.end() );
}

BOOST_AUTO_TEST_CASE( chunks_ints )
{
    std::vector<int> out, in = { 1, 2, 3, 5, 6, 8 };
    chunks( in.begin(), in.end(), [&](auto b, auto e) {
        out.push_back(*b);
    }, [](int a,int b) {
        return a%2==b%2;
    } );
    auto expected = { 1, 2, 3, 6 };
    BOOST_CHECK_EQUAL_COLLECTIONS( out.begin(), out.end(), expected.begin(), expected.end() );
}
