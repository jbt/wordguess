#include <wg/ProgramOptions.hpp>
#define BOOST_TEST_MODULE ProgramOptions
#include <boost/test/unit_test.hpp>

namespace {
    struct Fix
    {
        std::vector<const char*> args_ = { "wordguess" };
        std::ostringstream out_;
        wg::ProgramOptions parse()
        {
            return wg::ProgramOptions { static_cast<int>(args_.size()), args_.data(), out_ };
        }
    };
}

BOOST_AUTO_TEST_CASE(seed_defaults_to_zero)
{
    Fix f;
    BOOST_CHECK_EQUAL(f.parse().seed_,0U);
}
BOOST_AUTO_TEST_CASE(run_defaults_true)
{
    Fix f;
    BOOST_CHECK_EQUAL(f.parse().run_,true);
}

BOOST_AUTO_TEST_CASE(help_leads_to_immediate_exit)
{
    Fix f;
    f.args_.push_back( "--help" );
    BOOST_CHECK_EQUAL(f.parse().run_,false);
}
