#include "jbt/LineIO.hpp"
#define BOOST_TEST_MODULE LineIO
#include <boost/test/unit_test.hpp>

namespace {
    std::string escape(std::string s)
    {
        std::ostringstream rv;
        for ( auto c : s )
        {
            if ( std::isgraph(c) )
            {
                rv.put( c );
            }
            else
            {
                rv << '[' << short(c) << ']';
            }
        }
        return rv.str();
    }
}
BOOST_AUTO_TEST_CASE(ctor)
{
    std::stringstream ss;
    jbt::LineIO io { ss, ss };
}

BOOST_AUTO_TEST_CASE(inserts_spaces_and_newline_via_insertion_operator)
{
    std::istringstream i;
    std::ostringstream o;
    jbt::LineIO io { i, o };
    io << "The number is" << 4 << '.';
    auto actual = o.str();
    std::string expected = "The number is 4 . \n";
    BOOST_CHECK_EQUAL(actual,expected);
    BOOST_CHECK_EQUAL(actual.size(),expected.size());
    BOOST_CHECK_EQUAL(escape(actual),escape(expected));
}

BOOST_AUTO_TEST_CASE(out_behaves_the_same_as_insertion)
{
    {
        std::istringstream i;
        std::ostringstream o;
        jbt::LineIO io { i, o };
        io << 1 << 2;
        BOOST_CHECK_EQUAL(o.str(),"1 2 \n");
    }
    {
        std::istringstream i;
        std::ostringstream o;
        jbt::LineIO io { i, o };
        io .out() << 1 << 2;
        BOOST_CHECK_EQUAL(o.str(),"1 2 \n");
    }
}


BOOST_AUTO_TEST_CASE(line_maintains_case_at_first)
{
    std::istringstream i { "hello" };
    std::ostringstream o;
    jbt::LineIO io { i, o };
    BOOST_CHECK( io.read() );
    BOOST_CHECK_EQUAL( io.line(), "hello" );
    BOOST_CHECK_EQUAL( io.upper(), "HELLO" );
    //BOOST_CHECK_EQUAL( io.line(), "HELLO" ); Not asserted because it's not required
}
