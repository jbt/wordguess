#include <wg/Player.hpp>
#define BOOST_TEST_MODULE Player
#include <boost/test/unit_test.hpp>

using namespace wg;

BOOST_AUTO_TEST_CASE(display_pads_left)
{
    Player p { 9 };
    p.target("C");
    auto actual = p.display("A");
    BOOST_CHECK_MESSAGE(boost::icontains(actual,"        A < ?"),actual);
}
BOOST_AUTO_TEST_CASE(display_pads_right)
{
    Player p { 9 };
    p.target("C");
    auto actual = p.display("D");
    std::string expected = "            ? < D         ( D is alphabetically later than the random word )";
    BOOST_CHECK_EQUAL(actual.size(),expected.size());
    for ( auto i = 0; i < actual.size(); ++i )
    {
        auto j = actual.size() - i;
        BOOST_CHECK_EQUAL("<"+actual.substr(0,j)+">","<"+expected.substr(0,j)+">");
        BOOST_CHECK_EQUAL("<"+actual.substr(i  )+">","<"+expected.substr(i  )+">");
    }
    BOOST_CHECK_EQUAL(actual,expected);
    BOOST_CHECK_EQUAL(actual.size(),expected.size());
}

BOOST_AUTO_TEST_CASE(win_increments_wincount)
{
    Player p { 9 };
    BOOST_CHECK_EQUAL(p.win_count(),0UL);
    BOOST_CHECK_EQUAL(p.win(),1UL);
    BOOST_CHECK_EQUAL(p.win_count(),1UL);
}

BOOST_AUTO_TEST_CASE(target_round_trip)
{
    Player p { 8 };
    BOOST_CHECK_NE(p.target(),"ANEWTARGET");
    p.target( "ANEWTARGET" );
    BOOST_CHECK_EQUAL(p.target(),"ANEWTARGET");
}
