#include <wg/RunApp.hpp>
#define BOOST_TEST_MODULE RunApp
#include <boost/test/unit_test.hpp>

namespace {
    struct Fix
    {
        std::vector<jbt::fs::path> files_, directories_;
        bool include_default_dirs_ = true;
        std::uint_least32_t seed_ = 0U;
        bool debug_ = false;
        jbt::fs::path userwordsfile_ = "nope.txt";
        int called_ = 0;
        template<class A,class B,class C,class D>
        void operator()( A const& a, B const&b,C const&c,D const&d)
        {
            ++called_;
        }
    };
}

BOOST_AUTO_TEST_CASE(RunsGameNormally)
{
    Fix f;
    wg::RunApp( f, std::ref(f) );
    BOOST_CHECK_EQUAL(f.called_,1);
}
