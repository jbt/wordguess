#include <wg/WordCollector.hpp>
#define BOOST_TEST_MODULE WordCollector
#include <boost/test/unit_test.hpp>

#include <jbt/FileSystem.hpp>

using namespace wg;
using namespace boost::filesystem;

BOOST_AUTO_TEST_CASE(writes_collected_sorted)
{
    BOOST_TEST_MESSAGE("init");
    auto p = jbt::fs::temporary_file();
    BOOST_TEST_MESSAGE("temporary_file");
    // BOOST_TEST_MESSAGE(p);
    std::vector<std::string> v { "Yo", "Hey" };
    {
        try
        {
            WordCollector wc { p };
            try
            {
                wc.collect( v );
            }
            catch ( std::runtime_error const& re )
            {
                BOOST_FAIL( "collect" << re.what() );
            }
        }
        catch ( std::runtime_error const& re )
        {
            BOOST_FAIL( "ctor " << re.what() );
        }
    }
    BOOST_CHECK_EQUAL(v.size(),0UL);
    std::ifstream f { p.string() };
    std::string l;
    BOOST_CHECK(std::getline(f,l));
    BOOST_CHECK_EQUAL(l,"Hey");
    BOOST_CHECK(std::getline(f,l));
    BOOST_CHECK_EQUAL(l,"Yo");
    BOOST_CHECK(!std::getline(f,l));
}
