#include <wg/PlaySynchronously.hpp>
#define BOOST_TEST_MODULE PlaySynchronously
#include <boost/test/unit_test.hpp>
#include <map>

using namespace wg;

namespace {
    struct C : public std::map<std::string,int>
    {
       std::size_t index_of( auto whatever ) const { return 0UL; }
       std::pair<const std::string,int> const& at( int i ) const
       {
           return *std::next( begin(), i );
       }
    };
    namespace mock
    {
        struct Fix
        {
            using iterator = C::const_iterator;
            C w_;
            std::size_t size() const {
                return w_.size();
            }
            bool round() {
                return play_round(*this, *this, *this);
            }
            std::array<std::string,2> bounds() const { return {"",""}; }
            void target( std::string t ) {
                t_ = t;
            }
            std::string t_;
            template<class Auto,class Auto0>
            std::string select( Auto io, Auto0 p ) const {
                return "WORD2GUESS";
            }
            std::vector<std::string> output_, input_, guesses_taken_;
            template<class Auto>
            Fix& operator<<( Auto val )
            {
                std::ostringstream s;
                s << val;
                output_.push_back( s.str() );
                return *this;
            }
            Fix& out() {
                return *this;
            }
            bool read() {
                return input_.size();
            }
            std::string upper()
            {
                if ( input_.empty() )
                    return "";
                auto rv = input_.at(0);
                input_.erase( input_.begin() );
                return rv;
            }
            std::string line() {
                return upper();
            }
            int wins_ = 0;
            int win() {
                return ++wins_;
            }
            std::string target() const {
                return t_;
            }
            auto par() const {
                return 1;
            }
            template<class Auto,class Auto0,class Auto1,class Auto2>
            bool take_guess( Auto io, Auto0 guess, Auto1 tries, Auto2 player ) const
            {
                const_cast<std::vector<std::string>&>(guesses_taken_).push_back( guess );
                return guess == t_;
            }
            std::vector<bool> play_round_returns = { false, true, true };
            template<class Auto>
            void collect( Auto whatever ) const {}
            template<class Auto>
            std::pair<std::string,int> at(Auto i) const {
                return {"NOPE",9};
            }
            template<class Auto>
            bool contains( boost::string_ref w, Auto comparison ) const {
                return true;
            }
            auto const& words() const { return w_; }
            template<class Auto>
            int display( Auto const& ) {return 9;}
            template<class Auto>
            int register_guess( Auto const& ) {return 9;}
        };
        bool play_round( Fix const& game, Fix& player, Fix& io )
        {
            auto rv = player.play_round_returns.at( 0 );
            player.play_round_returns.erase( player.play_round_returns.begin() );
            return rv;
        }
    }
    using namespace mock;
}

BOOST_AUTO_TEST_CASE(play_round_apologizes_on_closed_stream)
{
    Fix f;
    BOOST_CHECK_EQUAL( false, f.round() );
    BOOST_CHECK_GE( f.output_.size(), 2 );
    BOOST_CHECK_MESSAGE( boost::icontains(f.output_.at(1),"sorry"), f.output_.at(1) );
}

BOOST_AUTO_TEST_CASE(play_round_acks_win)
{
    Fix f;
    f.t_ = "WORD2GUESS";
    f.input_.clear();
    f.input_.push_back( f.t_ );
    BOOST_CHECK_EQUAL(false,f.round());
    auto at = 1UL;
    BOOST_CHECK_GT( f.output_.size(), at );
    BOOST_CHECK_MESSAGE( boost::icontains(f.output_.at(at),"won"), f.output_.at(at) );
}

BOOST_AUTO_TEST_CASE(n_is_a_negative_response)
{
    Fix f;
    f.t_ = "WORD2GUESS";
    f.input_.clear();
    f.input_.push_back( f.t_ );
    f.input_.push_back( "N" );
    BOOST_CHECK_EQUAL(false,f.round());
    auto at = 1UL;
    BOOST_CHECK_GT( f.output_.size(), at );
    BOOST_CHECK_MESSAGE( boost::icontains(f.output_.at(at),"won"), f.output_.at(at) );
}

BOOST_AUTO_TEST_CASE(play_synchronously_loops_only_until_play_round_returns_false)
{
    Fix f;
    std::istringstream in;
    std::ostringstream out;
    //f.w_ = { {"HEY",1}, {"YOU",2} };
    f.w_["HEY"] = 1;
    f.w_["YOU"] = 2;
    auto called = 0;
    std::function<bool(Game<Fix,jbt::LineIO>&,Player&,jbt::LineIO&)> rnd = [&](Game<Fix,jbt::LineIO>&,Player&,jbt::LineIO&) {
        return++called<3;
    };
    play_synchronously( in, out, f, false, 9, f, rnd );
    BOOST_CHECK_EQUAL(called,3);
}
