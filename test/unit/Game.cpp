#include <wg/Game.hpp>
#define BOOST_TEST_MODULE Game
#include <boost/test/unit_test.hpp>

namespace ut = boost::unit_test;

#include <jbt/String.hpp>
#include <boost/filesystem.hpp>
#include <map>
#include <iostream>

namespace {
    struct Dict
    {
        using C = std::map<std::string,int>;
        C w_;
        using iterator = C::const_iterator;
        C::value_type const& at(int i) const
        {
//             BOOST_TEST_MESSAGE("at("<<i<<')');
            return *std::next(w_.begin(),i);
        }
        auto size() const {
            return w_.size();
        }
        template<class Auto>
        bool contains(std::string const&s,Auto comparator) const
        {
            for ( auto& p : w_ )
            {
                if ( !comparator(s,p) && !comparator(p,s) )
                    return true;
            }
            return false;
        }
        std::size_t frequency(short) const {
            return 1UL;
        }
        Dict const& underlying() const {
            return *this;
        }
    };
    struct Fixture
    {
        Dict dict_;
        Fixture( std::map<std::string,int> dictionary )
            : dict_ { {dictionary} }
        {
            for ( auto w : dictionary )
                resp_.push_back( w.first );
        }
        Fixture& out_stream() {
            return *this;
        }
        Fixture& out() {
            return *this;
        }
        std::ostringstream dbg_stream_;
        std::ostream& dbg() {
            return dbg_stream_;
        }
        std::vector<std::string> messages_written;
        template<class Auto>
        Fixture& operator<<( Auto const& val )
        {
            std::ostringstream s;
            s << val;
            messages_written.push_back( s.str() );
            return *this;
        }
        void write( std::string msg ) {
            BOOST_TEST_MESSAGE("write:"<<msg);
        }
        std::vector<std::string> resp_;
        std::string popped_;
        bool read()
        {
            popped_.clear();
            if ( resp_.empty() )
                return false;
            popped_ = resp_.at(0);
            resp_.erase(resp_.begin());
            return true;
        }
        const std::string& line() const
        {
            BOOST_TEST_MESSAGE("Fake input is:" << popped_ << '\n');
            return popped_;
        }
        std::string& upper() {
            return popped_;
        }
        std::size_t idx_ = 0UL;
        wg::Game<Dict,Fixture> testee_ { dict_ };
        std::string register_guess( std::string guess ) { return display(guess); }
        std::string display( std::string guess ) {
            return " |Guess="+guess+"|";
        }
        int tries_ = 0;
        std::string guess_ = "1 2.@\n\b";
        std::string target_ = "ONE";
        int win_count() const {
            return 90;
        }
        auto target() const {
            return target_;
        }
        template<class Auto>
        void target( Auto t ) {
            target_ = t;
        }
        std::uint_least32_t& seed() {
            return seed_;
        }
        std::uint_least32_t seed_ = 9U;
        bool take_guess()
        {
            boost::string_ref target = target_;
            auto rv = testee_.take_guess( *this, guess_, tries_, *this );
            target_ = target.to_string();
            return rv;
        }
    };
}

BOOST_AUTO_TEST_CASE( can_construct_fixture  )
{
    Fixture fix( Dict::C { {std::make_pair("ONE",1), std::make_pair("TWO",2), std::make_pair("THREE",3), std::make_pair("THRE",3)} } );
    BOOST_CHECK(true);
}
BOOST_AUTO_TEST_CASE( can_win )
{
    Fixture fix( Dict::C { {std::make_pair("ONE",1), std::make_pair("TWO",2), std::make_pair("THREE",3), std::make_pair("THRE",3)} } );
    fix.guess_ = fix.target_;
    BOOST_CHECK( fix.take_guess() );
    //BOOST_CHECK_MESSAGE( boost::contains(fix.str_.str(),"Congratulations"), fix.str_.str() );
    BOOST_CHECK_GE(fix.messages_written.size(),1);
    BOOST_CHECK_EQUAL("Congratulations! You guessed",fix.messages_written.at(0));
}
BOOST_AUTO_TEST_CASE( eventually_presents_all_the_words )
{
    Fixture fix( Dict::C { {std::make_pair("ZERO",0), std::make_pair("ONE",1), std::make_pair("TWO",1), std::make_pair("THREE",2)} } );
    BOOST_CHECK_EQUAL(fix.dict_.at(0).first,"ONE");
    BOOST_CHECK_EQUAL(fix.dict_.at(1).first,"THREE");
    BOOST_CHECK_EQUAL(fix.dict_.at(2).first,"TWO");
    BOOST_CHECK_EQUAL(fix.dict_.at(3).first,"ZERO");
    auto to_be_chosen = std::vector<std::string> { "ZERO", "ONE", "TWO", "THREE" };
    for ( auto count = 0UL; to_be_chosen.size(); ++count )
    {
        std::ostringstream log;
        auto t = fix.testee_.select( log, fix );
        auto i = std::find( to_be_chosen.begin(), to_be_chosen.end(), t );
        if ( i == to_be_chosen.end() )
        {
//             BOOST_TEST_MESSAGE( t << ' ' << to_be_chosen.size() << " left with " << count << " wins." );
        }
        else
        {
            count = 0UL;
            to_be_chosen.erase( i );
        }
        BOOST_REQUIRE_LT(count,99999);
    }
}
BOOST_AUTO_TEST_CASE( requires_more_than_one_word )
{
    try
    {
        Fixture f( Dict::C { {std::make_pair("HI",9)} } );
        BOOST_FAIL("An exception was expected.");
    }
    catch ( std::runtime_error const& re )
    {
        BOOST_CHECK(boost::icontains(re.what(),"dictionaries"));
    }
}
BOOST_AUTO_TEST_CASE( guessing_nonsense_counts_against_you )
{
    Fixture f( Dict::C { {std::make_pair("HI",99)}, {std::make_pair("WORD",0)} } );
    f.guess_ = "AAAAAAAAAAABCDEFG";
    BOOST_CHECK_EQUAL(f.tries_,0);
    BOOST_CHECK_EQUAL(f.take_guess(),false);
    BOOST_CHECK_EQUAL(f.tries_,1);
}
BOOST_AUTO_TEST_CASE( guessing_a_letter_doesnt_count )
{
    Fixture f( Dict::C { {std::make_pair("HI",99)}, {std::make_pair("WORD",0)} } );
    f.guess_ = "A";
    BOOST_CHECK_EQUAL(f.tries_,0);
    BOOST_CHECK_EQUAL(f.take_guess(),false);
    BOOST_CHECK_EQUAL(f.tries_,0);
}
BOOST_AUTO_TEST_CASE( guessing_wrong_but_valid_increments_tries )
{
    Fixture f( Dict::C { {std::make_pair("HI",99)}, {std::make_pair("WORD",0)} } );
    f.target_ = "WORD";
    f.guess_ = "HI";
    BOOST_CHECK_EQUAL(f.tries_,0);
    BOOST_CHECK_EQUAL(f.take_guess(),false);
    BOOST_CHECK_EQUAL(f.tries_,1);
}
BOOST_AUTO_TEST_CASE( guessing_nonalphas_doesnt_win_or_count_against_you )
{
    Fixture f { Dict::C{ {"ABC", 0U}, {"DEF",99U} } };
    f.target_ = f.guess_;
    BOOST_CHECK_EQUAL(f.tries_,0);
    BOOST_CHECK_EQUAL(f.take_guess(),false);
    BOOST_CHECK_EQUAL(f.tries_,0);
}
BOOST_AUTO_TEST_CASE( guessing_farther_out_counts_against_you )
{
    Fixture f( Dict::C { {"AA",0}, {"BB",0}, {"CC",99}, {"DD",0}
        , {"A0",0}, {"B9",0}, {"C0",99}, {"D9",0}
        , {"A1",0}, {"B8",0}, {"C1",99}, {"D8",0}
        , {"A2",0}, {"B7",0}, {"C2",99}, {"D7",0}
        , {"A3",0}, {"B6",0}, {"C3",99}, {"D6",0}
        , {"A4",0}, {"B5",0}, {"C4",99}, {"5D",0}
    } );
    f.target_ = "CC";
    BOOST_CHECK_EQUAL(f.tries_,0);
    f.guess_ = "DD";
    BOOST_CHECK_EQUAL(f.take_guess(),false);
    BOOST_CHECK_EQUAL(f.tries_,1);
    f.guess_ = "BB";
    BOOST_CHECK_EQUAL(f.take_guess(),false);
    BOOST_CHECK_EQUAL(f.tries_,2);
    f.guess_ = "AA";
    BOOST_CHECK_EQUAL(f.take_guess(),false);
    BOOST_CHECK_EQUAL(f.tries_,3);
}
BOOST_AUTO_TEST_CASE( guessing_correct_with_just_more_than_par_tries_is_good )
{
    Fixture f { Dict::C{ {"ABC", 0U}, {"DEF",99U}, {"ONE",0U} } };
    f.guess_ = f.target_;
    f.tries_ = f.testee_.par() + 1;
    BOOST_CHECK_EQUAL(f.take_guess(),true);
    BOOST_CHECK_GE(f.messages_written.size(),1);
    BOOST_CHECK_EQUAL(f.messages_written.at(f.messages_written.size()-1),"Good!");
}
