#include <jbt/String.hpp>
#define BOOST_TEST_MODULE String
#include <boost/test/unit_test.hpp>
#include <set>

BOOST_AUTO_TEST_CASE(string_less_ref)
{
    BOOST_CHECK_LT( std::string {"abc"}, boost::string_ref {"def"} );
}
BOOST_AUTO_TEST_CASE(ref_less_string)
{
    BOOST_CHECK_LT( boost::string_ref {"abc"}, std::string {"def"} );
}
BOOST_AUTO_TEST_CASE(string_eq_ref)
{
    BOOST_CHECK_EQUAL( std::string {"abc"}, boost::string_ref {"abc"} );
}
BOOST_AUTO_TEST_CASE(ref_eq_string)
{
    BOOST_CHECK_EQUAL( boost::string_ref {"abc"}, std::string {"abc"} );
}
/*
Ah, so the problem is actually with the container interface. The argument must be of the class's tparam's type, not just comparable to.
BOOST_AUTO_TEST_CASE(set_string_find_ref)
{
    std::set<std::string> s = { "abc", "def", "ghi" };
    std::size_t c = s.count(boost::string_ref{"jkl"});
    BOOST_CHECK_EQUAL(c,0UL);
//     BOOST_CHECK_EQUAL(s.count(boost::string_ref{"def"}),1UL);
}
*/
