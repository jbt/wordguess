#!/usr/bin/env python3

from lxml import html
from os import path
import glob
import os
import platform
import subprocess
import sys
import tempfile

def ut_gcda_dirs( bin_dir ):
    rv=[]
    for root, dirs, files in os.walk( bin_dir ):
        for f in files:
            #if path.splitext(f)[1] == '.gcda':
                #print( f, root, path.dirname(root), path.basename(path.dirname(root)) )
            if path.splitext(f)[1] == '.gcda' and path.basename(path.dirname(root)) == 'test':
                rv.append( root )
    return rv

def smoke_gcda_dir( bin_dir ):
    for root, dirs, files in os.walk( bin_dir ):
        if 'wordguess_cov' in root:
            for f in files:
                if f == 'main.cpp.gcda':
                    return path.join( root, f )
    raise FileNotFoundError
    
   
def generate_info( bin_dir, src_dir, gcda_dirs, outdir ):
   if ( len(gcda_dirs) < 1 ):
       raise Exception('Cannot generate info with no gcda stuffs!' )
   args = [ 'lcov', '--quiet', '--capture', '--no-external', '--base-directory', path.join( src_dir, 'src' ) ]
   for d in gcda_dirs:
       args += [ '--directory', d ]
   os.makedirs( outdir, exist_ok = True )
   args += [ '--output-file', path.join(outdir, 'cov.info') ]
   #print( ' '.join(args) )
   proc = subprocess.Popen( args, shell=False, stderr=subprocess.PIPE )
   proc.wait( 99 )
   for l in proc.stderr.readlines():
       if not 'geninfo: WARNING: no data found for ' in str(l) and not ' at /usr/bin/geninfo line 1281.' in str(l):
           print( l )
   if proc.returncode != 0:
       raise Exception( 'Process failed with exit code ' + str(proc.returncode) + ' ' + ' '.join(args) )
   
def generate_html( out_dir ):
   args = [ 'genhtml', '--quiet', '--show-details', '--num-spaces', '4', '--legend', '--demangle-cpp', path.join(out_dir,'cov.info'), '--output-directory', out_dir ]
   subprocess.check_call( args, stderr=subprocess.DEVNULL )

def browse( html ):
    print( 'Opening', html )
    if 'CYGWIN' in platform.system():
        subprocess.call( ['cygstart', html], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL )
    else:
        subprocess.call( ['xdg-open', html], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL )

def check_stats( cov_dir, src_dir ):
   min_cov = 99
   min_dir = ''
   for root, dirs, files in os.walk( cov_dir ):
      for f in files:
         p = path.join( root, f )
         if f == 'index.html':
            src_fn = root
            dom = html.parse( p )
            lines = dom.xpath( 'body/table/tr/td/table/tr/td[@class="headerItem" and text()="Lines:"]/../td[@class="headerCovTableEntryMed" or @class="headerCovTableEntryLo"]/text()' )
            #print( p, lines )
            if len( lines ) < 1:
                continue
            if len( lines[0] ) < 4:
                continue;
            cov_percent = float( lines[0][0:4] )
            if cov_percent <= min_cov:
                min_cov = cov_percent
                min_dir = p
            continue
         sp = path.splitext( f )
         if sp[1] != '.html':
            continue
         sp = path.splitext( sp[0] )
         if sp[1] != '.gcov':
            continue
         src_fn = sp[0]
         sp = path.splitext( src_fn )
         if sp[1] != '.hpp':
            continue
         dom = html.parse( p )
         lines = dom.xpath( 'body/table/tr/td/table/tr/td[@class="headerItem" and text()="Lines:"]/../td[@class="headerCovTableEntry"]/text()' )
         #print( p, lines )
         if int( lines[1] ) - int( lines[0] ) > 9:
            src = path.join( path.join(src_dir,'src'), path.relpath(path.join(root,src_fn), cov_dir) )
            print( src+':1:error: ', src, 'has insufficient line coverage:', lines[0],  'out of', lines[1], 'lines were hit.' )
            browse( p )
            raise Exception( src )
   if len(min_dir) > 0:
        print( min_cov, min_dir )
        src = path.dirname( min_dir )
        print( srd+'/itsthedirectory.cpp:1:error: ', src, 'has insufficient line coverage: ', min_cov, '%' )
        browse( min_dir )
        raise Exception( src )

def compare_linebyline( smoke_report, ut_report, src ):
    replace = False
    o=[]
    with open(smoke_report) as sr:
        with open(ut_report) as utr:
            s = sr.readlines()
            u = utr.readlines()
            if len(s) != len(u):
                print( src+':1:error:', 'It appears there is a mismatch, ', len(s), ' lines of code appear in the smoke test coverage report, but ', len(u), ' in the unit test coverage report.' )
                raise Exception( src )
            for l in zip(s,u):
                if '"lineCov"' in l[0] and not '"lineCov"' in l[1]:
                    print( src+':'+l[0].split('>')[1].split('<')[0].strip()+':error:', 'Smoke test covers this line but unit tests do not.' )
                    o.append( l[0].replace('lineCov', 'coverLegendCovLo') )
                    replace = True
                else:
                    o.append( l[1] )
    if replace and len(o) > 3:
        with open(ut_report, 'w') as utr:
            for l in o:
                utr.write( l )
            browse( ut_report )
            raise Exception( src )

def compare( ut_cov_dir, smoke_cov_dir, src_dir ):
    min_cov = 99
    min_dir = ''
    smoke_base = path.join( smoke_cov_dir, 'src' )
    #print( smoke_base )
    for root, dirs, files in os.walk( smoke_base ):
        for f in files:
            p = path.join( root, f )
            sp = path.splitext( f )
            if sp[1] != '.html':
                continue
            sp = path.splitext( sp[0] )
            if sp[1] != '.gcov':
                continue
            src_fn = sp[0]
            sp = path.splitext( src_fn )
            if sp[1] != '.hpp':
                continue
            rp = path.relpath( p, smoke_base )
            ut_p = path.join( ut_cov_dir, rp )
            source_path = path.join(src_dir, 'src', path.dirname(rp), src_fn)
            if path.isfile(ut_p):
                compare_linebyline( p, ut_p, source_path )
            else:
                print( source_path+':1:error: ', 'It would appear your unit tests completely missed a file.', p, 'exists but ', ut_p, 'does not.' )
                browse( p )
                raise Exception( ut_p )

        
if __name__ == '__main__':
    print( ' '.join(sys.argv) )
    bin_dir = sys.argv[1]
    src_dir = sys.argv[2]
    ut_out_dir = path.join(bin_dir, 'cov')
    generate_info( bin_dir, src_dir, ut_gcda_dirs(bin_dir), ut_out_dir )
    generate_html( ut_out_dir )
    check_stats( ut_out_dir, src_dir )
    smoke_out_dir = path.join(bin_dir, 'smoke_cov')
    generate_info( bin_dir, src_dir, [smoke_gcda_dir(bin_dir)], smoke_out_dir )
    generate_html( smoke_out_dir )
    compare( ut_out_dir, smoke_out_dir, src_dir )
