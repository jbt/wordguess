/**
 * @file
 * @copyright Copyright 2015 John Turpish © 2015 John Turpish
 *
 *  This file is part of WordGuess.
 *
 *  WordGuess is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  WordGuess is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with WordGuess.  If not, see http://www.gnu.org/licenses/.
 *
 * @brief program entry point
 * @note It is a laudable goal to keep this file particularly small. Unit testing it is a pain the neck and it is by design incredibly difficult to reuse code from here.
 * @author John Turpish
 * @date 2015-10-21
 */

#include "wg/WordCollector.hpp"
#include "wg/ProgramOptions.hpp"
#include "wg/PlaySynchronously.hpp"
#include "jbt/FileSystem.hpp"
#include "jbt/Range.hpp"
#include "jbt/For.hpp"
#include <boost/range/join.hpp>
#include <iostream>
#include <random>
#include <sys/types.h>
#include <sys/stat.h>

/**
 * @brief OS entry point
 * @param argc Argument count
 * @param argv Argument Vector
 * @return 0 for graceful termination, other values are various errors
 */
int main( int argc, char const* argv[] )
{
    try
    {
        wg::ProgramOptions po{argc, argv, std::cout};
        if ( !po.run_ )
        {
            return 0;
        }
        wg::Dictionary dict;
        auto& files = po.files_;
        auto df = wg::Dictionary::default_files();
        files.insert( files.end(), df.begin(), df.end() );
        auto& dirs = po.directories_;
        if ( po.include_default_dirs_ )
        {
            auto dd = wg::Dictionary::default_directories();
            dirs.insert( dirs.end(), dd.begin(), dd.end() );
        }
        std::set<uintmax_t> seen_sizes;
        jbt::For::cross( files, dirs, [&]( auto& f, auto& d )
                         {
                             auto p = d / f;
                             //Although this could have false positives and skip loading files that we shouldn't have, perhaps
                             //the odds of two different word lists having exactly the same number of bytes is pretty low.
                             //It's much more likely that it's a copy of the file we saw before, or a link to it.
                             if ( is_regular_file( p ) && seen_sizes.insert( file_size( p ) ).second )
                             {
                                 auto count = read( dict, p );
                                 if ( po.debug_ )
                                     std::cout << "Read " << count << " words out of " << p << '\n';
                             }
                         } );
        if ( po.seed_ == 0 )
        {
            /*
            std::random_device rd;
            seed = rd();
            Sorry, but I'm of the opinion that a silly little game like this should not be burning through physical entropy.
            Not even a couple bytes. If they want to try to play crazy games to predict the seed they can just use the --seed argument.
            */
            po.seed_ = std::time( 0 ) + ::getpid();
        }
        if ( po.debug_ )
            std::cout << "Seed is " << po.seed_ << ".\n";
        auto sorted = dict.finished();
        if ( po.debug_ )
        {
            std::cout << "Word obscurity histogram:\n";
            for ( auto o = 0UL; o < 99; ++o )
            {
                auto f = dict.frequency( o );
                if ( f )
                    std::cout << "   " << o << ") " << f << '\n';
            }
        }
        wg::WordCollector wc{po.userwordsfile_};
        wg::play_synchronously( std::cin, std::cout, sorted, po.debug_, po.seed_, wc, wg::play_round<wg::Game<decltype( sorted ), jbt::LineIO>&, wg::Player&, jbt::LineIO&> );
    }
    catch ( std::exception const& ex )
    {
        std::clog << std::endl << "ERROR: " << ex.what() << std::endl;
        return 2;
    }
    catch ( std::string const& s )
    {
        std::clog << std::endl << "DevError: " << s << std::endl;
        return 3;
    }
}

/**
 * @todo Write this
 * @todo a logo
 * @todo Extract more code out of main
 * @todo directory structure consider doc/html -> html and cov/ to html/cov
 * @todo timing test in Release builds 
 * @todo other interfaces
 * @todo detect missing tparam doxygen tags
 * @todo Look into uploading packages to repos
 *
 * @page hacking Hacking
 * There's a lot of assumptions about things you should have installed in your development environment.
 *
 * What              | Why
 * ------------------|----
 * binutils          | Apparently right now my build directly calls things like mv and which. It isn't necessary, this is the role Python should play. If you run into problems with this let me know.
 * cmake             | I want to force myself to use it.
 * cpack+            | CPack comes with CMake, but its generators call out to stuff you may also need. On non-Linux you probably already have what's necessary (Cygwin utility if on Cygwin, tar.gz on all platforms). If however your OS is Linux-based you are expected to have *BOTH* RPM (i.e. rpmbuild) and DEB tools (e.g. dpkg-deb) installed, which is actually rather rare.
 * doxygen           | Generates this here documentation, including lots of useful parsing-result stuff, nice formatting, and warnings so that we don't forget to document.
 * gcc/clang         | Some compiler flags are pretty specific and non-portable. Things like -fprofile-arcs. Sorry, but these are the only two compilers I know which will work.
 * lcov              | Which in turn depends upon gcov to do all the real work of generating code coverage information from test runs. lcov formats it into html files I prefer to look at. coverage.py also parses the html, which might be silly since clang-cov exists.
 * meld              | Invoked if the smoke test fails. If there's some other graphical diff program you prefer, patch it up bud.
 * make/ninja/etc.   | cmake can't do squat on its own; it needs a build system for which it can generate files.
 * python3           | Needed to pick something for build scripting. Python seems like the most portable and comfortable option. I've gotten used to Ruby of late, but it's not installed nearly as widely.
 * ronn              | Generates the man page based on the README.md
 * xdg-open/cygstart | Not technically required, but you'll see unfortunate errors rather than pretty reports (the argument is always .html). If neither works for you let's figure out what would and how to detect that environment.
 * valgrind          | The build will go on without it, but if you DO have it the tests will run under memcheck (and fail if there's an error) and that's awesome. In the future I might add some performance testing and may want to use cachegrind (or callgrind I suppose).
 *
 * A note on compiler versions: using something quite recent. I use quite a bit of C++11, some C++14, and a tiny bit of C++17, which may go away.
 *
 * @par Project Setup
 *      One of the stranger choices here is the active rejection of the separate compilation model.
 *      You've heard of header-only libraries. This is a nigh-header-only application.
 *      If you write a .cpp file it has to define main (or link in a library that defines main, i.e. is a test suite).
 *      There's lots of pros and cons to this. The main motivating pro here is obviating the brokenness of popular build systems like CMake.
 *      It's also interesting, even good, that it makes the biggest costs  of C++ generics (templates, auto params, concepts) basically a sunk cost.
 *      So I, and presumably you, will feel at peace with using them super freely. Let's.
 *      The biggest downside is build times, but this thing has so little code in the first place it's really not a problem yet. If it becomes a problem we can look into precompiled headers.
 *
 *      Don't include more than one header from the project in a test suite. They're unit tests not integration tests.
 *
 *      The graph of header interdependencies should be extremely straightforward...
 *
 *    * Try to minimize including project headers in other project headers. In particular try to use a template or runtime parameter for anything you mighr reasonably want to mock.
 *    * Don't include a header from a parent directory.
 *    * Don't include headers from a higher-level directory. In particular headers in wg/ may freely include headers from jbt/ but the reverse should never happen.
 *    * Minimize including headers from the same directory.
 *
 *      Use double-quote includes when including headers from the same directory or subdirectories (and make the path relative to the current file's dir). Use angle brackets otherwise.
 *
 *      jbt/ is for code that isn't specific to this application, and in fact at some point may move to a separate project for reuse.
 *
 *      Minimize the size of main().
 *
 *      Namespaces should, preferably, match directories.
 *
 *      Not super strict on naming conventions, perhaps less consistent than I should be:
 *
 *    * Data members end in _
 *    * Types are usually InitialUpperCamel, but a typedef that one could just as easily have been a part of std may match those conventions. (i.e. seed_t)
 *    * Most other names are underscore_separated_all_lower.
 *    * Constants are usually UNDERSCORE_SEPARATED_ALL_UPPER. This flies in the face of recommendation from Core Guidelines, and is definitely up for debate.
 */
