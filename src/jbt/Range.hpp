/**
 * @file
 * @copyright Copyright 2015 John Turpish © 2015 John Turpish
 *
 *  This file is part of WordGuess.
 *
 *  WordGuess is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  WordGuess is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with WordGuess.  If not, see http://www.gnu.org/licenses/.
 *
 * @brief Contains some range utilities
 * @author John Turpish
 * @date 2015-10-25
 */

#ifndef RANGE_HPP_INCLUDED
#define RANGE_HPP_INCLUDED 1

#include "String.hpp"
#include <stdexcept>
#include <algorithm>

namespace jbt
{

    /**
     * @brief Presents functionality that's valid on a sorted range
     * @details Normally constructed by the factory certify_sorted
     */
    template <class Range>
    class SortedRangeView
    {
    public:
        using iterator = typename Range::const_iterator; ///< Iteration order should follow the sort order. Likely a random-access iterator. Not guaranteed, but if it isn't there's gonna be some ugly performance degradation.

        /**
         * @brief Constructs an adaptor for some utilities
         * @details Doesn't modify underlying range which must remain valid
         * @pre [b,e) must already be sorted
         * @param underlying The underlying container
         */
        SortedRangeView( Range& underlying )
        : under_( underlying )
        {
            using std::begin;
            using std::end;
            begin_ = begin( under_ );
            end_   = end( under_ );
#if NDEBUG
            if ( !std::is_sorted( begin_, end_ ) )
            {
                throw std::invalid_argument( "Range is not sorted! " + BOOST_CURRENT_FUNCTION );
            }
#endif
        }

        /**
         * @brief How many elements are contained
         * @return end() - begin()
         */
        std::size_t size() const
        {
            using std::distance;
            return static_cast<std::size_t>( distance( begin_, end_ ) );
        }

        /**
         * @brief Check whether the given value is present in the range
         * @details This overload assumes std::less determines sort order
         * @param value The value being looked for
         * @return true iff it is
         */
        template <class Value>
        bool contains( Value const& value ) const
        {
            return contains( value, std::less<Value>{} );
        }

        /**
         * @brief Check whether the given value is present in the range
         * @param value The value being looked for
         * @param comparitor functor defining sort order
         * @return true iff it is
         */
        template <class Auto, class Auto0>
        bool contains( Auto const& value, Auto0 comparitor ) const
        {
            using std::binary_search;
            return binary_search( begin_, end_, value, comparitor );
        }

        /**
        * @brief Like operator[] but with bounds checking
        */
        auto& at( std::size_t index ) const
        {
            if ( index >= size() )
                throw std::out_of_range( jbt::to_string( index ) + ">=" + jbt::to_string( size() ) + " in " + BOOST_CURRENT_FUNCTION );
            return ( *this )[index];
        }

        /**
         * @brief access the element offset by index
         * @param index offset from start of range
         * @pre index >= 0
         * @pre index < size()
         * @return reference to the element
         */
        auto& operator[]( std::size_t index ) const
        {
            using std::next;
            return *next( begin_, index );
        }

        /**
         * @brief Access the underlying range
         */
        Range const& underlying() const { return under_; }

       template<class Value>
       std::size_t index_of( Value const& val ) const
       {
          using std::distance;
          using std::lower_bound;
          return distance( begin_, lower_bound(begin_, end_, val) );
       }
    
    private:
        Range const& under_;
        iterator begin_, end_;
    };

    /**
    * @brief Assume range is sorted and construct a SortedRangeView from it
    * @note At the moment it's presumed to be sorted by operator<
    * @todo Allow caller to pass in predicate for sort order
    * @param range the underlying range which must outlive the return
    * @return SortedRangeView which encapsulates algos that rely on the order
    */
    template <class Range>
    auto certify_sorted( Range& range )
    {
        return SortedRangeView<Range>( range );
    }
}

#endif
