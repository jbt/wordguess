/**
 * @file
 * @copyright Copyright 2015 John Turpish © 2015 John Turpish
 *
 *  This file is part of WordGuess.
 *
 *  WordGuess is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  WordGuess is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with WordGuess.  If not, see http://www.gnu.org/licenses/.
 *
 * @brief Home of jbt::ArbitraryUnsigned
 * @author John Turpish
 * @date 2015-10-30
 */

#ifndef ARBITRARY_UNSIGNED_HPP_INCLUDED
#define ARBITRARY_UNSIGNED_HPP_INCLUDED 1

#include <unistd.h>
#include <ctime>
#include <random>
#include <unordered_map>

namespace jbt
{

    /**
     * @brief Maintains thread-local pseudorandom sequence
     */
    template <class U>
    struct ArbitraryUnsignedThreadState
    {

        /**
         * @brief Intialize the state of this generator
         * @param seed Seed to initialize the state with, or zero if no good seed available
         */
        ArbitraryUnsignedThreadState( std::uint_least32_t seed )
        : dre_{seed ? seed : std::random_device{}()}
        {
        }
        std::default_random_engine dre_; ///< A random number generator. Encapsulates the pseudorandom sequence.
        std::unordered_map<U, std::uniform_int_distribution<U>> dists_; ///< Integer distributions for various maximum values
    };

    /**
     * @brief Access a potentially randomish number
     * @param N The value returned must be less than N (and at least zero)
     * @param seed If not already initialized in this thread, seed will be used if not zero. If zero physical entropy is used.
     * @return An integer in the range [0,N)
     */
    template <class U>
    U ArbitraryUnsigned( U N, std::uint_least32_t seed = 0U )
    {
        static thread_local ArbitraryUnsignedThreadState<U> s{seed};
        return s.dists_.emplace( N, std::uniform_int_distribution<U>{0U, N - 1U} ).first->second( s.dre_ );
    }
}

#endif
