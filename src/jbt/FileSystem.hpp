/**
 * @file
 * @copyright Copyright 2015 John Turpish © 2015 John Turpish
 *
 *  This file is part of WordGuess.
 *
 *  WordGuess is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  WordGuess is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with WordGuess.  If not, see http://www.gnu.org/licenses/.
 *
 * @brief Home of namespace jbt::fs
 * @author John Turpish
 * @date 2015-10-22
 */

#ifndef FILE_SYSTEM_HPP_INCLUDED
#define FILE_SYSTEM_HPP_INCLUDED 1

#include "ArbitraryUnsigned.hpp"

#define BOOST_SYSTEM_NO_DEPRECATED 1 ///< Excludes some code that causes warnings. Wouldn't want to use deprecated functions anyhow.
#include <boost/filesystem.hpp>

namespace jbt
{

    /**
     * @brief Utilities for interacting with the file system.
     * @details Blatantly based on Boost.Filesystem, adding some anciliary stuff that undoubtedly one couldn't get enough consensus on to put into Boost proper.
     */
    namespace fs
    {
        using boost::filesystem::path;

        /**
         * @brief Attempt to return a user's directory. Fallback to the current directory.
         * @return @verbatim ${HOME} , %USERPROFILE% , possbily other stuff as time goes on @endverbatim whichever is the first to exist, or . if nothing else is found
         */
        path home()
        {
            path p;
            auto env = std::getenv( "HOME" );
            if ( env && is_directory( p = env ) )
                return p;
            env = std::getenv( "USERPROFILE" );
            if ( env && is_directory( p = env ) )
                return p;
            return ".";
        }

        /**
         * @brief Get a path suitable for a new temporary file.
         */
        path temporary_file()
        {
            using namespace boost::filesystem;
            auto d = temp_directory_path();
#ifdef __CYGWIN__
            std::string f( ArbitraryUnsigned( 64UL ), 'A' );
            for ( auto& c : f )
            {
                c = ArbitraryUnsigned( static_cast<unsigned>( 25U ) ) + 'A';
            }
#else
            auto f = unique_path();
#endif
            return d / f;
        }
    }
}

#endif
