/**
 * @file
 * @copyright Copyright 2015 John Turpish © 2015 John Turpish
 *
 *  This file is part of WordGuess.
 *
 *  WordGuess is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  WordGuess is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with WordGuess.  If not, see http://www.gnu.org/licenses/.
 *
 * @brief Home of jbt::LineIO
 * @author John Turpish
 * @date 2015-11-01
 */

#ifndef LINE_INPUT_HPP_INCLUDED
#define LINE_INPUT_HPP_INCLUDED 1

#include "String.hpp"

#include <iostream>
#include <sstream>

namespace jbt
{
    /**
    * @brief Abstraction for line-based synchronous IO
    * @details This is intended to be a wrapper around std::istream and std::ostream that implements a concept which I haven't clearly defined, but is generally described in the brief.
    */
    class LineIO
    {
    public:
        /**
         * @brief Construct from the underlying streams
         * @param in underlying input stream
         * @param out underlyoutg output stream
         * @param debug Whether debug messages should be written to the output stream
         */
        explicit LineIO( std::istream& in, std::ostream& out, bool debug = false )
        : in_( in )
        , out_( out )
        , debug_{debug}
        {
        }

        /**
         * @brief Scope-based control for a single message
         */
        struct MessageWriter
        {
            using Flusher = std::function<void( std::string const& )>; ///< A functor which may be called when it's time to end a message

            /**
             * @brief Construct for a message
             * @param buffer Where the message is to be written
             * @param flusher What is to be done when it's finished
             */
            MessageWriter( std::ostringstream& buffer, Flusher flusher )
            : buffer_( buffer )
            , flush_( flusher )
            {
            }
            ~MessageWriter()
            {
                auto s = buffer_.str();
                if ( s.size() )
                    flush_( s );
            }

            MessageWriter( MessageWriter const& ) = delete; ///< Nope
            MessageWriter& operator=( MessageWriter const& ) = delete; ///< Nope
            MessageWriter( MessageWriter&& ) = default; ///< Yup

            /**
             * @brief Add a value to the message
             * @param val What to add
             * @return *this
             */
            template <class Auto>
            MessageWriter& operator<<( Auto&& val )
            {
                buffer_ << val << ' ';
                return *this;
            }

        private:
            std::ostringstream& buffer_;
            Flusher flush_;
        };

        /**
         * @brief access an output stream that triggers on scope end
         */
        MessageWriter out()
        {
            buffer_.str( {} );
            return { buffer_, [this]( std::string const& s )
                     {write(s);
                     } };
        }

        /**
         * @brief Get the current debug stream
         * @return Output stream if debug output is enabled, null stream otherwise
         */
        MessageWriter dbg()
        {
            if ( debug_ )
            {
                buffer_.str( {} );
                return {
                    buffer_,
                    [this](std::string const&s){write(s);}
                };
            }
            return {
                buffer_,
                []( std::string const& s ){s.size();}
            };
        }

        /**
         * @brief Convenience wrapper for code that expects std::ostream
         * @param val First value to write in this output message
         * @return A stream for the rest of the message
         */
        template <class Auto>
        MessageWriter operator<<( Auto const& val )
        {
            buffer_.str( {} );
            buffer_ << val << ' ';
            return { 
                buffer_,
                [this]( std::string const& s ){write(s);}
            };
        }

        /**
        * @brief Directly request input from the user (or source of IO)
        * @param question Text to present to the user indicating what is required.
        * @return Whether there was a response
        * @post line() contains response if true is returned.
        */
        bool prompt( boost::string_ref question )
        {
            write( question );
            return read();
        }

        /**
         * @brief Receive input into the input stream
         * @return true iff input was available
         */
        bool read()
        {
            line_.clear();
            return std::getline( in_, line_ ) && !line_.empty();
        }

        /**
        * @brief Access the most recent line of input
        */
        std::string const& line() const
        {
            return line_;
        }

        /**
        * @brief Access the most recent line of input, but in all uppercase
        */
        std::string& upper()
        {
            boost::to_upper( line_ );
            return line_;
        }

    private:
        std::istream& in_; ///< The input source
        std::ostream& out_; ///< The output destination
        std::string line_; ///< A line of input
        std::ostringstream buffer_; ///< Buffer for output messages while building.
        bool debug_; ///< Whether we're writing dbg to out_

        void write( boost::string_ref msg )
        {
            out_ << msg << std::endl;
        }
    };
}

#endif
