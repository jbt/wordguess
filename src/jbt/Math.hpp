/**
 * @file
 * @copyright Copyright 2015 John Turpish © 2015 John Turpish
 *
 *  This file is part of WordGuess.
 *
 *  WordGuess is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  WordGuess is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with WordGuess.  If not, see http://www.gnu.org/licenses/.
 *
 * @brief Wrapper around cmath with workarounds
 * @author John Turpish
 * @date 2015-11-02
 */

#include <cmath>

namespace jbt
{
#ifdef __CYGWIN__
    template <class Numeric>
    inline auto log2( Numeric x ) -> decltype( std::log( x ) )
    {
        return std::log( x ) / std::log( 2 );
    }
#else
    using std::log2;
#endif
}
