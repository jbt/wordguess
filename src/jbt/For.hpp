/**
 * @file
 * @copyright Copyright 2015 John Turpish © 2015 John Turpish
 *
 *  This file is part of WordGuess.
 *
 *  WordGuess is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  WordGuess is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with WordGuess.  If not, see http://www.gnu.org/licenses/.
 *
 * @brief Home of namespace jbt::For
 * @author John Turpish
 * @date 2015-10-23
 */

#ifndef FOR_HPP_INCLUDED
#define FOR_HPP_INCLUDED 1

#include <algorithm>

namespace jbt
{

    /**
     * @brief collection of simplistic looply algos
     * @details Comparable to std::for_each, these iterator- and/or range- based algorithms execute a provided functor in a way reminiscent of a for loop (or two). Generally the functors called here are allowed to have return values, but what they are is irrelevant as they are ignored.
     */
    namespace For
    {

        /**
         * @brief Cross product loop.
         * @details Execute functor on every possible combination of the elements of major_range and minor_range. Similar to a for nested in a for.
         * @param major_range The major access gets iterated over exactly once
         * @param minor_range The minor access gets iterated over for each element of major_range
         * @param functor is a binary function or function object called for each combination, major_range providing the first parameter and minor_range the second. Return value, if any, is ignored.
         * @throw ... exceptions are propagated agnostically from functor
         */
        template <class Auto, class Auto0, class Auto1>
        void cross( Auto&& major_range, Auto0&& minor_range, Auto1 functor )
        {
            for ( auto&& major_element : major_range )
            {
                for ( auto&& minor_element : minor_range )
                {
                    functor( major_element, minor_element );
                }
            }
        }

        /**
         * @brief conceptually split the range on a delimiter and do something for each in-between group
         * @details The type of delimiter should be comparable to the type achieved by *begin_it, since it is these values it is assumed to delimit. begin_it should be comparable to end_it as these form a range. Functor is being called for each range of elements between delimiters or range ends, for example if the range is [ 1, 2, 0, 3, 4, 0, 0, 5, 6 ] and the delimiter is 0 functor would be called with the ranges [ 1, 2 ]; [ 3, 4 ]; []; and [ 5, 6 ]
         * @param begin_it The start of the input range. Its type should be convertible to both parameter types of functor.
         * @param end_it past-the-end of the input range. Its type should be convertible to the type of the second parameter of functor.
         * @param delimiter The value upon which to split
         * @param functor what to do for other values. functor should take two arguments which are [begin, end) iterators. The first param will always be either begin_it or something derived from some number of incremements of begin_it. The second param will sometimes be of the same type as the first, and up to once the same type as end_it. The return value, if any, is ignored.
         * @throw ... exceptions are propagated agnostically from functor
         */
        template <class Auto, class Auto0, class Auto1, class Auto2>
        void delim( Auto begin_it, Auto0 end_it, Auto1 delimiter, Auto2 functor )
        {
            while ( begin_it != end_it )
            {
                auto delim_it = std::find( begin_it, end_it, delimiter );
                functor( begin_it, delim_it );
                if ( delim_it == end_it )
                    return;
                begin_it = std::next( delim_it );
            }
        }

        /**
        * @brief Iterate over all elements of [begin_it,end_it) a subrange at a time
        * @details Unlike delim the subranges determined by predicate are strictly adjacent
        * @param begin_it Start of the input range
        * @param end_it Past the end of the input range
        * @param functor What to call with each subrange. Parameters are begin, end. Return is ignored.
        * @param predicate returns true for each element of the subrange past the first. Arguments are first element and Nth element.
        */
        template <class Auto, class Auto0, class Auto1, class Auto2>
        void chunks( Auto begin_it, Auto0 end_it, Auto1 functor, Auto2 predicate )
        {
            while ( begin_it != end_it )
            {
                auto diff_it = std::find_if_not( begin_it, end_it, [begin_it,&predicate](auto&x){return predicate(*begin_it,x);} );
                functor( begin_it, diff_it );
                begin_it = diff_it;
            }
        }
    }
}

#endif
