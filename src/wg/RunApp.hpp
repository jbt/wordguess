/**
 * @file
 * @copyright Copyright 2015 John Turpish © 2015 John Turpish
 *
 *  This file is part of WordGuess.
 *
 *  WordGuess is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  WordGuess is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with WordGuess.  If not, see http://www.gnu.org/licenses/.
 *
 * @brief program entry point
 * @note It is a laudable goal to keep this file particularly small. Unit testing it is a pain the neck and it is by design incredibly difficult to reuse code from here.
 * @author John Turpish
 * @date 2015-11-19
 */

#include "WordCollector.hpp"
#include "Dictionary.hpp"
#include <jbt/For.hpp>
#include <iostream>
#include <random>
#include <ctime>
#include <sys/types.h>
#include <sys/stat.h>

namespace wg
{
    
    /**
     * @brief Run the application
     * @param po Program options
     * @param local_play Function to call to initiate playing with a user on std::cin
     * @return 0
     */
    template<class Options,class LocalPlay>
    int RunApp( Options const& po, LocalPlay local_play )
    {
        wg::Dictionary dict;
        auto files = po.files_;
        auto df = wg::Dictionary::default_files();
        files.insert( files.end(), df.begin(), df.end() );
        auto dirs = po.directories_;
        if ( po.include_default_dirs_ )
        {
            auto dd = wg::Dictionary::default_directories();
            dirs.insert( dirs.end(), dd.begin(), dd.end() );
        }
        std::set<uintmax_t> seen_sizes;
        jbt::For::cross( files, dirs, [&]( auto& f, auto& d )
                         {
                             auto p = d / f;
                             //Although this could have false positives and skip loading files that we shouldn't have, perhaps
                             //the odds of two different word lists having exactly the same number of bytes is pretty low.
                             //It's much more likely that it's a copy of the file we saw before, or a link to it.
                             if ( is_regular_file( p ) && seen_sizes.insert( file_size( p ) ).second )
                             {
                                 auto count = read( dict, p );
                                 if ( po.debug_ )
                                     std::cout << "Read " << count << " words out of " << p << '\n';
                             }
                         } );
        auto seed = po.seed_;
        if ( seed == 0 )
            seed = static_cast<decltype(seed)>( std::time( nullptr ) + ::getpid() );
        if ( po.debug_ )
            std::cout << "Seed is " << seed << ".\n";
        auto sorted = dict.finished();
        if ( po.debug_ )
        {
            std::cout << "Word obscurity histogram:\n";
            for ( unsigned short o = 0U; o < 99; ++o )
            {
                auto f = dict.frequency( o );
                if ( f )
                    std::cout << "   " << o << ") " << f << '\n';
            }
        }
        wg::WordCollector wc{po.userwordsfile_};
        local_play( sorted, po.debug_, seed, wc );
        return 0;
    }
}
