/**
 * @file
 * @copyright Copyright 2015 John Turpish © 2015 John Turpish
 *
 *  This file is part of WordGuess.
 *
 *  WordGuess is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  WordGuess is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with WordGuess.  If not, see http://www.gnu.org/licenses/.
 *
 * @brief Home of wg::Player
 * @author John Turpish
 * @date 2015-10-28
 */

#ifndef PLAYER_HPP_INCLUDED
#define PLAYER_HPP_INCLUDED 1

#include <jbt/String.hpp>
#include <vector>

namespace wg
{

    /**
     * @brief Maintains the state of a single player's interaction with the game.
     */
    class Player
    {
    public:
        /**
         * @brief Construct a player who may use seed for a pseudorandom sequence
         */
        explicit Player( std::uint_least32_t seed )
        : seed_{seed}
        {
        }

        /**
         * @brief Get how many times this player has won.
         */
        std::size_t win_count() const { return wins_; }

        /**
         * @brief Record a new win!
         * @return The new win_count()
         */
        std::size_t win() { return ++wins_; }

        std::string register_guess( std::string const& current_guess )
        {
           past_guesses_.emplace_back( current_guess );
           return display( current_guess );
        }
        
        /**
         * @brief Display string contextualizing the guess for the user
         * @param current_guess The miss that prompted this
         * @return roughly low_ < ? < high_
         */
        std::string display( std::string const& current_guess )
        {
            if ( current_guess < target_ )
            {
                if ( low_.empty() || current_guess > low_ )
                    low_.assign( current_guess );
            }
            else
            {
                if ( high_.empty() || current_guess < high_ )
                    high_.assign( current_guess );
            }
            std::string rv;
            if ( low_.size() )
            {
                spaces( rv, low_.size() );
                rv.append( low_ );
                rv.append( " < " );
            }
            else
            {
               spaces( rv, 0UL );
               rv.append( "   " );
            }
            rv.push_back( '?' );
            if ( high_.size() )
            {
                rv.append( " < " );
                rv.append( high_ );
                spaces( rv, high_.size() );
            }
            else
            {
               spaces( rv, 0UL );
               rv.append( "   " );
            }
            rv.append( " ( " ).append( current_guess ).append( " is alphabetically " );
            if ( current_guess < target_ )
            {
                rv.append( "earlier" );
            }
            else
            {
                rv.append( "later" );
            }
            return rv.append( " than the random word )" );
        }

        /**
         * @brief Indicate to *this that its player should be guessing a different word now
         * @param target The new word-to-be-guessed
         * @note Impacts display
         */
        void target( boost::string_ref const& target )
        {
            low_.clear();
            high_.clear();
            target_ = target;
        }

        /**
         * @brief Access the word current being guessed at
         */
        boost::string_ref const& target() const { return target_; }

        /**
         * @brief Get an integer appropriate for seeding a pseudorandom number generator
         */
        std::uint_least32_t& seed() { return seed_; }

        /**
         * @brief Some words this player has guessed.
         * @details This is unlikely to contain correct guesses, but it would be
         *     perfectly well-behaved if it did. It's mostly used to populate
         *     userwords.txt
         * @return modifiable reference to recent guesses
         */
        std::vector<std::string>& past_guesses() { return past_guesses_; }
        
        std::array<std::string,2> bounds() const { return {low_, high_}; }

    private:
        static constexpr std::size_t MIN_WIDTH = 9UL;

        std::uint_least32_t seed_;
        std::size_t wins_ = 0UL;
        std::string high_, low_;
        boost::string_ref target_;
        std::vector<std::string> past_guesses_;

        static void spaces( std::string& out, std::size_t N )
        {
            if ( MIN_WIDTH > N )
            {
                out.append( MIN_WIDTH - N, ' ' );
            }
        }
    };
}

#endif
