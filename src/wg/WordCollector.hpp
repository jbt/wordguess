/**
 * @file
 * @copyright Copyright 2015 John Turpish © 2015 John Turpish
 *
 *  This file is part of WordGuess.
 *
 *  WordGuess is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  WordGuess is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with WordGuess.  If not, see http://www.gnu.org/licenses/.
 *
 * @brief Home of wg::WordCollector
 * @author John Turpish
 * @date 2015-10-26
 */

#include <boost/current_function.hpp>
#include <boost/filesystem.hpp>

#include <iostream>
#include <fstream>
#include <mutex>
#include <string>
#include <set>
#include <iterator>

namespace wg
{
    /**
     * @brief Persists some user-entered words.
     */
    class WordCollector
    {
    public:
        /**
        * @brief Collect a wordcollector who reads and writes to path_to_userwords
        * @param path_to_userwords path to the userwords file.
        */
        WordCollector( boost::filesystem::path const& path_to_userwords )
        : path_{path_to_userwords.string()}
        , words_{}
        {
            if ( is_directory( path_to_userwords.parent_path() ) )
            {
                std::ifstream in{path_};
                std::string word;
                while ( in >> word )
                    words_.insert( word );
            }
            else
            {
                create_directories( path_to_userwords.parent_path() );
            }
        }
        ~WordCollector()
        {
            try
            {
                Lock lck{mtx_, std::defer_lock};
                if ( attempt_lock( lck, std::chrono::seconds( 1 ) ) )
                {
                    std::ofstream out{path_};
                    std::ostream_iterator<std::string> o{out, "\n"};
                    std::copy( words_.begin(), words_.end(), o );
                }
                else
                {
                    std::clog << BOOST_CURRENT_FUNCTION << " was unable to achieve its own lock!" << std::endl;
                }
            }
            catch ( ... )
            {
            }
        }

        /**
        * @brief Add the contents of recorded to the collection, clear out recorded
        */
        template <class Auto>
        void collect( Auto& recorded )
        {
            if ( recorded.empty() )
                return;
            using std::begin;
            using std::end;
            Lock lck{mtx_, std::defer_lock};
            if ( attempt_lock( lck, std::chrono::milliseconds( 100 ) ) )
            {
                words_.insert( begin( recorded ), end( recorded ) );
                recorded.clear();
            }
            else
            {
                std::clog << BOOST_CURRENT_FUNCTION << " is becoming a bottleneck!" << std::endl;
            }
        }

    private:
#ifdef __CYGWIN__
        using Mutex = std::mutex;
        template <class Auto, class Auto0>
        static bool attempt_lock( Auto& lck, Auto0 timeout )
        {
            try
            {
                lck.lock();
            }
            catch ( std::runtime_error const& re )
            {
                throw std::runtime_error( std::string( "Error while attempting to acquire lock in " ) + BOOST_CURRENT_FUNCTION + " : " + re.what() );
            }
            return timeout >= std::chrono::seconds( 0 );
        }
#else
        using Mutex = std::timed_mutex;
        template <class Auto, class Auto0>
        static bool attempt_lock( Auto& lck, Auto0 timeout )
        {
            return lck.try_lock_for( timeout );
        }
#endif
        using Lock = std::unique_lock<Mutex>;

        std::string path_;
        std::set<std::string> words_;
        Mutex mtx_;
    };
}
