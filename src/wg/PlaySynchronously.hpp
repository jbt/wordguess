/**
 * @file
 * @copyright Copyright 2015 John Turpish © 2015 John Turpish
 *
 *  This file is part of WordGuess.
 *
 *  WordGuess is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  WordGuess is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with WordGuess.  If not, see http://www.gnu.org/licenses/.
 *
 * @brief Home of wg::play_synchronously
 * @author John Turpish
 * @date 2015-10-27
 */

#ifndef PLAY_SYNCHRONOUSLY_HPP_INCLUDED
#define PLAY_SYNCHRONOUSLY_HPP_INCLUDED 1

#include "Dictionary.hpp"
#include "Game.hpp"
#include "Player.hpp"

#include <jbt/LineIO.hpp>

#include <iosfwd>

namespace wg
{

    /**
     * @brief collect a guess and provide a response
     * @param game the rules engine
     * @param player The user's state
     * @param io Textual interaction with the user
     * @return Whether the player wishes to start another round
     */
    template <class GameType, class PlayerType, class IOType>
    bool play_round( GameType const& game, PlayerType& player, IOType& io )
    {
        player.target( game.select( io, player ) );
        io << "Guess away!";
        io.read();
        std::string guess = io.line();
        auto tries        = 1;
        auto lost = true;
        for ( ; tries / game.par() < 5; guess = io.line() )
        {
            if ( boost::starts_with(guess, "+hint") )
            {
               auto& ws = game.words();
               auto lohi = player.bounds();
               if ( lohi[0].empty() || lohi[1].empty() || lohi[0] == lohi[1] )
               {
                  io.out() << "Guess a little more before getting a hint.";
               }
               else
               {
                  auto x = std::numeric_limits<unsigned short>::max();
                  auto l = ws.index_of( std::make_pair(lohi[0],x) );
                  x = 0;
                  auto h = ws.index_of( std::make_pair(lohi[1],x) );
                  std::string tgt{ player.target().data(), player.target().size() };
                  auto t = ws.index_of( std::make_pair(tgt,x) );
                  if ( t - l > h - t && l + 1 < t )
                  {
                     io.out() << player.display(  ws.at(l + 1).first );
                  }
                  else if ( h - 1 > t )
                  {
                     io.out() << player.display( ws.at(h - 1).first );
                  }
                  else
                  {
                     io.out() << "You're out of hints.";
                  }
               }
               ++tries;
               if ( io.read() )
               {
                  continue;
               }
               else
               {
                  break;
               }
            }
            if ( game.take_guess( io, guess, tries, player ) )
            {
                io.out() << "You have now won" << player.win() << "times!";
                lost = false;
                break;
            }
            //             if ( !guess.empty() )
            //                 collected_.insert( guess );//TODO: reimplement as its own abstraction, with synchronization
            if ( !io.read() )
                break;
        }
        if ( lost )
        {
            io.out() << "Sorry you failed to guess:" << player.target();
        }
        io.out() << "Play again? (y/n)";
        if ( !io.read() )
            return false;
        auto retry = io.upper();
        if ( retry.empty() )
            return false;
        if ( retry[0] != 'N' )
            return true;
        if ( retry.size() == 1 )
            return false;
        return retry.size() == 2 && retry[1] == 'O';
    }

    /**
     * @brief Long-running top-level loop handling one user interacting with the game until they give up
     * @param in Stream to read user input from
     * @param out Stream to write messages for the user to
     * @param dictionary Collection of words
     * @param debug Whether to write debugging messages to out
     * @param seed Value to initialize a pseudrandom sequence
     * @param word_collector Place to dump user-entered words to
     * @param one_round Collects a guess and provides a response.
     */
    template <class Dict, class Auto, class Auto0>
    void play_synchronously( std::istream& in, std::ostream& out, Dict const& dictionary, bool debug, std::uint_least32_t seed, Auto& word_collector, Auto0 one_round )
    {
        jbt::LineIO io{in, out, debug};
        Game<Dict, jbt::LineIO> game{dictionary};
        Player player{seed};
        while ( one_round( game, player, io ) )
        {
            if ( debug )
                out << "Collecting " << player.past_guesses().size() << " words.\n";
            word_collector.collect( player.past_guesses() );
        }
        if ( debug )
            out << "Collecting " << player.past_guesses().size() << " words.\n";
        word_collector.collect( player.past_guesses() );
    }
}

#endif
