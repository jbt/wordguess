/**
 * @file
 * @copyright Copyright 2015 John Turpish © 2015 John Turpish
 *
 *  This file is part of WordGuess.
 *
 *  WordGuess is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  WordGuess is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with WordGuess.  If not, see http://www.gnu.org/licenses/.
 *
 * @brief Home of wg::Game
 * @author John Turpish
 * @date 2015-10-24
 */

#ifndef WORDGUESS_GAME_HPP_INCLUDED
#define WORDGUESS_GAME_HPP_INCLUDED 1

#include <jbt/ArbitraryUnsigned.hpp>
#include <jbt/FileSystem.hpp>
#include <jbt/String.hpp>
#include <jbt/Math.hpp>

#include <fstream>
#include <random>
#include <set>
#include <algorithm>

namespace wg
{

    /**
     * @brief Encapsulation of game logic
     * @todo The modifiable game state (not the startup stuff) should be in a different class. Makes it much more extensible (think concurrent players).
     */
    template <class Words, class IO>
    class Game
    {
    public:
        /**
         * @brief Construct the game rule engine
         * @param dict The word-lookup data structure
         */
        explicit Game( Words const& dict )
        : dict_{dict}
        {
            if ( dict.size() < 2 )
                throw std::runtime_error( "Something is wrong with your setup. Check your dictionaries." );
        }
        ~Game() = default;

        /**
         * @brief Handle a user's guess - the most fundamental step of the game
         * @param out An output stream to which messages to the user are written
         * @param[in] guess The word the user guessed. Modified in-situ for formatting (trim, uppercase)
         * @param[in,out] tries How many guesses the user has taken (might get incremented)
         * @param[in,out] player Game state
         * @return Whether the guess was correct
         */
        template <class Auto, class Auto0>
        bool take_guess( Auto& out, std::string& guess, int& tries, Auto0& player ) const
        {
            auto i = std::remove_if( guess.begin(), guess.end(), [](char c) {
                return!std::isalpha(c);
            } );
            if ( i == guess.begin() )
            {
                out << "Please enter a word that is your guess. It should be comprised of letters.";
                return false;
            }
            if ( i == std::next(guess.begin()) )
            {
                out << ( "Your guess is too short. Try again." );
                return false;
            }
            guess.erase( i, guess.end() );
            boost::to_upper( guess );
            if ( !dict_.contains(guess, key_compare {}) )
            {
                out << guess << "is not a word I know. Try again.";
                ++tries;
                return false;
            }
            if ( guess == player.target() )
            {
                out << "Congratulations! You guessed" << player.target() << "in" << tries << "guesses." << score_characterization( tries );
                return true;
            }
            out << player.register_guess(guess);
            ++tries;
            out << "Your guess?";
            return false;
        }

        /**
         * @brief What is a 'good' score given the size of the word list?
         */
        std::size_t par() const
        {
            return std::max( 1UL, static_cast<std::size_t>(std::log(dict_.size())) );
        }

        /**
         * @brief Choose a random word
         * @param out A stream to which we can write user messages
         * @param player The user for whom we are selecting this word
         * @return A reference to the selected word *inside* dict_
         * @throw std::runtime_error if many, many attempts to select an appropriately-obscure word failed (likely indicates programmatic error)
         */
        template<class Auto,class Auto0>
        boost::string_ref select( Auto& out, Auto0& player ) const
        {
            for ( auto max_obscurity = player.win_count() / 2; max_obscurity <= 54321; ++max_obscurity )
            {
                auto pref = std::max( 1, preference(max_obscurity, player.win_count()) );
                for ( auto tries = 0; tries <= pref; ++tries )
                {
                    auto const& choice = dict_.at( jbt::ArbitraryUnsigned(dict_.size(), player.seed()) );
                    if ( choice.second <= max_obscurity )
                    {
                        out << "I have selected a word that is" << choice.first.size() << "letters long; obscurity level is" << choice.second << '.';
                        out << "See if you can guess it in fewer than " << ( par() + choice.second ) << " tries!";
                        return choice.first;
                    }
                }
            }
            throw std::runtime_error( std::string(BOOST_CURRENT_FUNCTION) + " appears to have gone infinite." );
        }
        
        Words const& words() const { return dict_ ; }

    private:
        Words const& dict_;

        char const* score_characterization( std::size_t guesses ) const
        {
            switch ( (guesses - 1) / par() )
            {
            case 0:
                return "Excellent!";
            case 1:
                return "Good!";
            case 2:
                return "Fair.";
            case 3:
                return "Poor.";
            default:
                return "What are you doing?";
            }
        }
        int preference( unsigned obscurity, int wins ) const
        {
            return ( 99 + obscurity ) / std::max( wins, 1 );
        }

        struct key_compare
        {
            using word_iterator = typename Words::iterator;
            using obscure_t = decltype(word_iterator {}->second);
            bool operator() ( std::string const& s, std::pair<std::string,obscure_t> const& p ) const {
                return s < p.first;
            }
            bool operator() ( std::pair<std::string,obscure_t> const& p, std::string const& s ) const {
                return p.first < s;
            }
        };
    };
}

#endif
