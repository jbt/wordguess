/**
 * @file
 * @copyright Copyright 2015 John Turpish © 2015 John Turpish
 *
 *  This file is part of WordGuess.
 *
 *  WordGuess is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  WordGuess is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with WordGuess.  If not, see http://www.gnu.org/licenses/.
 *
 * @brief Home of wg::ProgramOptions
 * @author John Turpish
 * @date 2015-10-29
 */

#include <Version.hpp>
#include <boost/program_options.hpp>
#include <jbt/FileSystem.hpp>
#include <stdexcept>

namespace wg
{
    /**
     * @brief Command line arguments
     */
    class ProgramOptions
    {
    public:
        using Paths  = std::vector<boost::filesystem::path>; ///< A collection of paths, string identifiers for objects in a filesystem
        using seed_t = std::uint_least32_t; ///< Integer type used to seed pseudorandom number generators

        bool run_ = true; ///< Whether this set of options should result in the game being run, rather than program exiting.
        Paths directories_; ///< Directories the user would like to search for directories possibly in addition to the normal ones
        Paths files_; ///< Dictionary filenames the user would like to be searched for in addition to the hard-coded ones.
        bool include_default_dirs_; ///< Whether the 'normal' guesses should be included in the search path for dictionaries
        bool debug_; ///< Whether debugging output should be written to stdout
        seed_t seed_; ///< A primer for a pseudorandom sequence, or 0 if it has yet to be determined
        boost::filesystem::path userwordsfile_; ///< Dicitonary file to which user-entered words should be saved

        /**
         * @brief Parse the options as if they were passed from main and populate member variables
         * @param argc Argument count. Should always be positive
         * @param argv Argument vector. Array of pointers to nul-terminated char arrays, which should have argc such pointers. argv[0] is ignored here.
         * @param log A stream to which we can write feedback for the user
         */
        ProgramOptions( int argc, const char* argv[], std::ostream& log )
        {
            using namespace boost::program_options;
            options_description desc{"Options"};
            // clang-format off
            desc.add_options()
		        ( "help,h"     , "This help message." )
		        ( "version,v"  , "Display version information." )
		        ( "directory,d", value<Paths>()->default_value(Paths(),""), "A directory to search for directories before the default path." )
		        ( "file,f"     , value<Paths>()->default_value(Paths(),""), "A file name to look for in each --directory before the default ones." )
		        ( "no-system-directories,n", "Do not check the normal locations; rely solely on --directory." )
		        ( "userwords"  , value<boost::filesystem::path>()->default_value(jbt::fs::home() / ".local" / "share" / "dict" / "userwords.txt"), "Where to save user-entered words.")
		        ( "seed"     , value<seed_t>()->default_value(0U), "For Debugging purposes.." )
		        ( "debug"      ,                                               "For debugging wordguess" )
            ;
            // clang-format on
            variables_map opts;
            store( command_line_parser( argc, argv ).options( desc ).run(), opts );
            if ( opts.count( "help" ) )
            {
                log << desc << '\n';
                run_ = false;
                return;
            }
            if ( opts.count( "version" ) )
            {
                log << VERSION_STRING << '\n';
                run_ = false;
                return;
            }
            notify( opts );
            try
            {
                directories_ = opts["directory"].as<Paths>();
            }
            catch ( boost::bad_any_cast& bac )
            {
                throw std::runtime_error( "Failed to convert --directory option to path: " + std::string( bac.what() ) );
            }
            include_default_dirs_ = 0 == opts.count( "no-system-directories" );
            if ( !include_default_dirs_ && directories_.empty() )
            {
                throw std::runtime_error( "If you use --no-system-directories you must also use at least one --directory.\n" );
            }
            files_         = opts["file"].as<Paths>();
            debug_         = opts.count( "debug" ) > 0;
            seed_          = opts["seed"].as<seed_t>();
            userwordsfile_ = opts["userwords"].as<boost::filesystem::path>();
        }
    };
}