/**
 * @file
 * @copyright Copyright 2015 John Turpish © 2015 John Turpish
 *
 *  This file is part of WordGuess.
 *
 *  WordGuess is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  WordGuess is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with WordGuess.  If not, see http://www.gnu.org/licenses/.
 *
 * @brief Home of wg::Dictionary
 * @author John Turpish
 * @date 2015-10-31
 */

#ifndef DICTIONARY_HPP_INCLUDED
#define DICTIONARY_HPP_INCLUDED 1

#include <jbt/FileSystem.hpp>
#include <jbt/String.hpp>
#include <jbt/For.hpp>
#include <jbt/Range.hpp>

#include <boost/current_function.hpp>

#include <fstream>
#include <vector>
#include <algorithm>

namespace wg ///< namespace for stuff explicitly specific to WordGuess
{

    /**
    * @brief A class that manages a collection of words.
    */
    class Dictionary
    {
    public:
        using obscure_t      = unsigned short; ///< Integer type for storing obscurity ratings. Get it? obscure "t". Obscurity.
        using Collection     = std::vector<std::pair<std::string, obscure_t>>; ///< Container for words and their associated obscurity ratings.
        using iterator       = Collection::const_iterator; ///< Iterator into Collection (as would be used in the interface)
        using const_iterator = Collection::const_iterator; ///< Alias for Iterator. In case some algo wants it.

        Dictionary() = default; ///< Nothing to see here.

        /**
         * @brief Add a word to the dictionary, if it meets criteria.
         * @details word must be all upper-case characters, at least two of them. Otherwise add has no effect.
         * @param word The word to add to the dictionary.
         */
        void add( std::string const& word )
        {
            if ( std::all_of( word.begin(), word.end(), ::isupper ) && word.size() > 1 )
            {
                words_.emplace_back( word, max_obscurity_ );
            }
        }

        /**
         * @brief Read in the stream of words, adding them to the dictionary
         * @details Words are expected to be one-per-line. Lines that don't meet the criteria of Dicitonary::add will be ignored.
         * @param stream Where to read the words from
         * @return How many lines were observed (regardless of whether they were added)
         */
        std::size_t read( std::istream& stream )
        {
            std::string line;
            auto rv = 0UL;
            ++max_obscurity_;
            while ( std::getline( stream, line ) )
            {
                boost::to_upper( line );
                add( line );
                ++rv;
            }
            return rv;
        }

        /**
         * @brief Assert that no further modifications will occur
         * @return a view providing both random-access and associative access.
         */
        jbt::SortedRangeView<Dictionary> finished()
        {
            std::sort( words_.begin(), words_.end() );
            auto o = words_.begin();
            jbt::For::chunks( words_.begin(), words_.end(),
                              [&o, this]( auto b, auto e )
                              {
                                  o->first  = b->first;
                                  o->second = b->second * max_obscurity_ - std::distance( b, e );
                                  ++o;
                              },
                              []( auto& a, auto& b )
                              {
                                  return a.first == b.first;
                              } );
            words_.erase( o, words_.end() );
            for ( auto& w : words_ )
            {
                if ( histogram_.size() <= w.second )
                    histogram_.resize( w.second + 1 );
                histogram_[w.second]++;
            }
            return jbt::certify_sorted( *this );
        }

        /**
         * @brief How many words have the given obscurity
         * @param o The obscurity to count for
         * @return A count of words
         */
        std::size_t frequency( obscure_t o ) const
        {
            if ( o >= histogram_.size() )
                return 0UL;
            return histogram_.at(o);
        }

        /**
         * @brief begin iterator for the range of all contained words
         */
        auto begin() const {
            return words_.begin();
        }

        /**
         * @brief past-the-end iterator for the range of all contained words
         */
        auto end() const {
            return words_.end();
        }

        /**
         * @brief Get the filenames we think dictionaries normally have
         */
        static std::vector<boost::filesystem::path> default_files()
        {
            return {
                {"userwords.txt"},
                {"verycommonwords.txt"},
                {"commonwords.txt"},
                {"words.txt"},
                {"words"},
                {"american-english"},
                {"american"},
                {"canadian-english"},
                {"brittish-english"},
                {"rarewords.txt"}
            };
        }

        /**
         * @brief Get the directories we think dictionaries can normally be found in
         */
        static std::vector<boost::filesystem::path> default_directories()
        {
            std::vector<boost::filesystem::path> rv;
            rv.push_back( jbt::fs::home() / ".local" / "share" / "dict" );
            rv.push_back( "data" );
            rv.push_back( "../data" );
            rv.push_back( "/usr/local/share/dict" );
            rv.push_back( "/usr/local/dict" );
            rv.push_back( "/usr/share/dict" );
            rv.push_back( "/usr/dict" );
            return rv;
        }

    private:
        Collection words_;
        obscure_t max_obscurity_ = 0U;
        std::vector<std::size_t> histogram_;

    };

    /**
     * @brief Read a dictionary file in
     * @param dict the collection to hold the words
     * @param file path to the file to be read
     * @return Count of apparent lines in the file
     */
    template<class Auto>
    std::size_t read( Auto& dict, boost::filesystem::path const& file )
    {
        if ( ! is_regular_file(file) )
            return 0UL;
        std::ifstream dict_file { file.string() };
        if ( !dict_file )
            throw std::runtime_error( "Couldn't open " + file.string() );
        return dict.read( dict_file );
    }
}

#endif
