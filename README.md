README.md
=========

WordGuess 
=========

![](wg.png)

INTRO
-----

A simplistic greater-than, less-than word guessing game.

Like when someone tells you to guess a number and after each guess tells you higher or lower... but with words.

Only words comprised entirely of A-Z are included. Internationalization would be a nightmare. At least the data structure doesn't make alphabetic assumptions _any more_.

A few dictionaries are included. Some attempt is made at locating other dictionaries on your system. You can also specify on the command line additional directories to search and, if you like, additional file names to look for in this dicitonary path. Show me where to look. Tell me what I'll find.

The expected number of guesses is based on the dicitonaries read in, primarily the number of unique words present.

USAGE
-----

None of the options are mandatory, except that if one uses --no-system-directories one must also use at least one --directory.

*  --help                  Display option summary
*  --directory DIRECTORY   A directory to search for directories before the default path. This option may occur more than once.
*  --file FILE             A file name to look for in each --directory  before the default ones. This option may occur more than once.
*  --no-system-directories Do not check the normal locations; rely solely on --directory options.

A couple options undermine the integrity of the game, and thus should not be used by normal players. They're useful when you're developing the game itself.

*  --seed  SEED For repeatable word choices.
*  --debug Enable debugging output
*  --userwords Where to dump user-entered words. If you're doing this during a real playing of the game you should pick somewhere that will be discovered as a dictionary file on subsequent runs.

SEE ALSO
--------
[Hacking](hacking.html) is useful if you plan to modify the program, especially if you'd like to cooperate with me. This link would work if you were looking at the HTML output of Doxygen.
